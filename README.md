# DTN COMET

This is the repository for the Delay- and Disruption-tolerant Networking Comprehensive Operational Metrics Evaluation Toolkit (DTN-COMET) created as part of my Master's Thesis at TU Dresden.

DTN-COMET enables conducting automated and repeatable performance evaluations of Delay-tolerant Networking protocol implementations such as [µD3TN](https://gitlab.com/d3tn/ud3tn) and [ION-DTN](https://github.com/nasa-jpl/ION-DTN). During these evaluations the following metrics are collected:
- Round-Trip Times
- Goodput between two nodes
- Memory and CPU utilization
- Bundle Retention Times
- Bundle serialization and deserialization times

DTN-COMET consists of two main components: the API for starting evaluations and querying evaluation results and the testbed nodes that perform the evaluations. The setup and configuration of each of these components is documented in a README, which is located in the respective subdirectory of the component.

Additionally, more information on how DTN-COMET works and how to get started using it can be found in the [wiki](https://gitlab.com/noethlich/dtn-comet/-/wikis/home).
