import json
import logging
import sys
import datetime
from typing import List

from utils.custom_logger import CustomFormatter
from utils.database_utils import connection_manager

# Setup logging
log_handler = logging.StreamHandler(sys.stdout)
log_handler.setLevel(logging.DEBUG)
log_handler.setFormatter(CustomFormatter())

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.propagate = False
logger.addHandler(log_handler)


class ResultsStore:
    def __init__(self):
        self.db_path = "results.db"

    def save_evaluation(self, mem_cpu, goodput, rtts, brts) -> dict:
        with connection_manager(self.db_path) as conn:
            cur = conn.cursor()
            logger.debug("Saving evaluation results.")

            current_datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            cur.execute(
                "INSERT INTO evaluations (date) VALUES (?)", (current_datetime,)
            )
            conn.commit()

            id = cur.lastrowid
            cur.execute(
                "INSERT INTO testruns (evaluation_id, type, data) VALUES (?,?,?)",
                (id, "mem_cpu", mem_cpu),
            )
            cur.execute(
                "INSERT INTO testruns (evaluation_id, type, data) VALUES (?,?,?)",
                (id, "goodput", goodput),
            )
            cur.execute(
                "INSERT INTO testruns (evaluation_id, type, data) VALUES (?,?,?)",
                (id, "rtts", json.dumps(rtts)),
            )
            cur.execute(
                "INSERT INTO testruns (evaluation_id, type, data) VALUES (?,?,?)",
                (id, "brts", brts),
            )
            conn.commit()

            logger.debug("Created evaluation.")

    def delete_evaluation(self, id: str):
        with connection_manager(self.db_path) as conn:
            cur = conn.cursor()
            logger.debug("Processing deletion request for evaluation %s", id)

            cur.execute("DELETE FROM evaluations WHERE id LIKE (?)", (id,))
            cur.execute("DELETE FROM testruns WHERE evaluation_id LIKE (?)", (id,))
            conn.commit()
            logger.debug("Deleted evaluation.")

    def get_all_evaluations(self) -> List[dict]:
        with connection_manager(self.db_path) as conn:
            cur = conn.cursor()
            cur.execute("SELECT * FROM evaluations")
            res = cur.fetchall()
            return res

    def get_evaluation(self, eval_id):
        results = {
            "date": "",
            "mem_cpu": {},
            "goodput": {},
            "rtts": [],
            "brts": {},
        }
        with connection_manager(self.db_path) as conn:
            cur = conn.cursor()
            cur.execute(
                "SELECT date FROM evaluations WHERE id LIKE (?)", (int(eval_id),)
            )
            res = cur.fetchone()
            if res:
                results["date"] = res[0]
                cur.execute(
                    "SELECT data FROM testruns WHERE evaluation_id LIKE (?) AND type LIKE (?)",
                    (
                        int(eval_id),
                        "mem_cpu",
                    ),
                )
                results["mem_cpu"] = json.loads(cur.fetchone()[0])
                cur.execute(
                    "SELECT data FROM testruns WHERE evaluation_id LIKE (?) AND type LIKE (?)",
                    (
                        int(eval_id),
                        "goodput",
                    ),
                )
                results["goodput"] = json.loads(cur.fetchone()[0])
                cur.execute(
                    "SELECT data FROM testruns WHERE evaluation_id LIKE (?) AND type LIKE (?)",
                    (
                        int(eval_id),
                        "rtts",
                    ),
                )
                results["rtts"] = json.loads(cur.fetchone()[0])
                cur.execute(
                    "SELECT data FROM testruns WHERE evaluation_id LIKE (?) AND type LIKE (?)",
                    (
                        int(eval_id),
                        "brts",
                    ),
                )
                results["brts"] = json.loads(cur.fetchone()[0])

            return results


results_store = ResultsStore()
