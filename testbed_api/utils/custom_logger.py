import logging

class CustomFormatter(logging.Formatter):
    def __init__(self, log_format="%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)", *args, **kwargs):
        super().__init__(log_format, *args, **kwargs)
    
    def formatMessage(self, record):
        message = super().formatMessage(record)
        return message