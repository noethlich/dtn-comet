import os
import re
import subprocess
import pandas as pd
import numpy as np
import scipy.stats as st


def load_data(path: str):
    with open(path, "r") as file:
        return file.read()


def process_cpu_memory_by_payload_size(log_node_1, cpu_mem_node_1, cpu_mem_node_2) -> pd.DataFrame:
    """
    This function processes timestamps between two goodput measurements recorded in log_node_1
    and retrieves corresponding entries from cpu_mem_node_1 and cpu_mem_node_2.

    Args:
        log_node_1: Log recorded during the evaluation. Contains entries in format 'START[1-15}]:timestamp' whenever a new goodput measurement run started.
        cpu_mem_node_1: List of CPU/memory usage entries for node 1
        cpu_mem_node_2: List of CPU/memory usage entries for node 2

    Returns:
        list: List of lists, where each inner list contains CPU/memory entries between consecutive START timestamps in log_node_1
    """
    log_node_1 = log_node_1.splitlines()
    cpu_mem_node_1 = cpu_mem_node_1.splitlines()
    cpu_mem_node_2 = cpu_mem_node_2.splitlines()
    labels = [1, 10, 25, 50, 75] + list(range(100, 1001, 100))

    for i in range(len(cpu_mem_node_1)):
        cpu_mem_node_1[i] = cpu_mem_node_1[i].split(",")

    for i in range(len(cpu_mem_node_2)):
        cpu_mem_node_2[i] = cpu_mem_node_2[i].split(",")

    tuple_list = []
    result = {}
    start_timestamps = []

    # Extract timestamps from START entries
    for entry in log_node_1:
        if entry.startswith("START"):
            _, timestamp = entry.split(":")
            start_timestamps.append(int(timestamp))

    # separate the two measurement runs
    run1 = start_timestamps[:150]
    run2 = start_timestamps[150:]
    start_timestamps = []

    # ensure that the start times are ordered by payload size across runs
    # -> [1kb_run1, 10kb_run1, ... , 1kb_run2, 10kb_run2, ...] -> [1kb_run1, 1kb_run2, 10kb_run1, 10kb_run2, ...]
    for i in range(0, len(run1), 10):
        start_timestamps.extend(run1[i : i + 10])
        start_timestamps.extend(run2[i : i + 10])

    for i in range(len(start_timestamps)):
        start_time = start_timestamps[i]

        if i <= len(start_timestamps) - 2 and (i + 1) % 10 != 0:
            end_time = start_timestamps[i + 1]
        elif i >= len(start_timestamps) - 2 or i == len(start_timestamps) - 11:
            end_time = (
                start_timestamps[i] + 90
            )  # Add 90 seconds to last entry as there is no subsequent "START"
        elif (i + 1) % 10 == 0:
            end_time = start_timestamps[i + 1 + 10]

        # Filter CPU/memory entries between current start and end time
        node_1_entries = [
            entry for entry in cpu_mem_node_1 if start_time <= int(entry[0]) < end_time
        ]
        node_2_entries = [
            entry for entry in cpu_mem_node_2 if start_time <= int(entry[0]) < end_time
        ]

        # Calculate CPU and memory usage for each entry
        node_1_usage = calculate_cpu_mem_usage(node_1_entries)
        node_2_usage = calculate_cpu_mem_usage(node_2_entries)

        # Combine entries from both nodes
        combined_entries = (node_1_usage, node_2_usage)
        tuple_list.append(combined_entries)

    # 300 entries (2 runs with 15 payload sizes, goodput being measured 10 times each)
    for i in range(len(tuple_list)):
        # 10 goodput measurements for payload size X in run 1 and 10 in run 2
        # -> after 20 entries a new payload size begins and we have to initialize the new dict
        if i % 20 == 0:
            result[labels[(i // 10) // 2]] = {
                "cpu_usage_sender": tuple_list[i][0]["cpu_usage"],
                "memory_usage_sender": tuple_list[i][0]["memory_usage"],
                "cpu_usage_receiver": tuple_list[i][1]["cpu_usage"],
                "memory_usage_receiver": tuple_list[i][1]["memory_usage"],
            }
        else:
            result[labels[(i // 10) // 2]]["cpu_usage_sender"] += tuple_list[i][0][
                "cpu_usage"
            ]
            result[labels[(i // 10) // 2]]["memory_usage_sender"] += tuple_list[i][0][
                "memory_usage"
            ]
            result[labels[(i // 10) // 2]]["cpu_usage_receiver"] += tuple_list[i][1][
                "cpu_usage"
            ]
            result[labels[(i // 10) // 2]]["memory_usage_receiver"] += tuple_list[i][1][
                "memory_usage"
            ]
    
    return pd.DataFrame(result)


def calculate_cpu_mem_usage(entries):
    """
    This function extracts CPU and memory usage for a list of entries.

    Args:
        entries: List of CPU/memory usage entries

    Returns:
        dict: Dictionary containing CPU and memory usage for the provided entries
    """
    cpu_usage = []
    memory_usage = []
    for entry in entries:
        cpu_delta_container = float(entry[1])
        cpu_delta_system = float(entry[2])
        cpu_usage.append((cpu_delta_container / cpu_delta_system) * 100 * 2)

        used_mem = int(entry[5]) + int(entry[6]) + int(entry[7])
        memory_usage.append(used_mem / 1e6)  # Conversion to MB

    return {"cpu_usage": cpu_usage, "memory_usage": memory_usage}


def process_goodput_by_payload_size(node2_log):
    """
    This function groups goodput measurements by payload size from the log file of the receiving node
    in the performance evaluation testbed.

    Args:
        node2_log (str): The content of the log file of node 2 as a string.

    Returns:
        pd.DataFrame: A DataFrame containing goodput measurements for each payload size.

            Index: Payload size in KBytes (1, 10, 25, ..., 1000)
            Column: Series of goodput measurements (Mbps) for each payload size.
    """
    # Find all occurrences of "Throughput (Mbps): XXXX.YYYY" in the log
    matches_throughput = [
        float(i) for i in re.findall(r"Throughput \(Mbps\): (\d+\.\d+)", node2_log)
    ]

    # chunk the data based on the number of measurements per payload size (default: 10)
    chunks_tp = [
        matches_throughput[x : x + 10] for x in range(0, len(matches_throughput), 10)
    ]

    # Combine chunks from the two full evaluation runs so all measurements for one payload size
    # are combined in one list.
    chunks_tp = [chunks_tp[i] + chunks_tp[i + 15] for i in range(0, 15)]

    data_tp = {}

    # explicitly group by payload size in kbytes
    labels = ["1", "10", "25", "50", "75", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000"]

    for i in range(len(chunks_tp)):
        data_tp[labels[i]] = chunks_tp[i]

    df = pd.DataFrame(data_tp)

    n_list = df.count(axis=0)
    sem_list = df.std(axis=0) / np.sqrt(n_list)

    t_value_99 = st.t.ppf(1 - (1 - 0.99) / 2, df=n_list.iloc[0] - 1)
    stats_dict = df.describe().to_dict()

    y = [x["mean"] for col, x in stats_dict.items()]
    e = [t_value_99 * sem_list[col] for col in stats_dict.keys()]  # Calculate 99% confidence interval

    data = np.array([[""]+labels,
                     ["avg_goodput"]+y,
                     ["confidence_interval"]+e])

    return pd.DataFrame(data=data[1:, 1:], index=data[1:, 0], columns=data[0, 1:])


def process_rtt_measurements(sender_log):
    """
    This function extracts the round-trip times measured during the evaluation from the log file
    of the sending node.

    Args:
        sender_log (str): The content of the log file of node 1.

    Returns:
        List[float]: A list containing the round-trip times measured during the evaluation in ms.
    """
    return [float(i) * 1000 for i in re.findall(r"LATENCY (\d+\.\d+)", sender_log)]


def process_bundle_retention_times(node2_brt_script, perf_data):
    """
    This function calculates the average bundle retention time in relation to the payload size.
    It also calculates the average serialization and deserialization times to investigate the
    impact they have on bundle retention.

    Args:
        node2_brt_script (str): Path to the Python script used by perf to calculate the BRTs.
        perf_data (str): Path to the data captured by perf on the middle node.

    Returns:
        pd.DataFrame: A DataFrame containing the following information for each payload size:

              Index: Payload size in KBytes (1, 10, 25, ..., 1000)

              Columns:

                - 'avg_bundle_retention_time': Average bundle retention time (seconds) for that payload size.
                - 'avg_serialization_time': Average serialization time (seconds) for that payload size.
                - 'avg_deserialization_time': Average deserialization time (seconds) for that payload size.
                - 'bundle_retention_confidence_interval': 99% confidence interval for the average bundle retention time.
                - 'serialization_confidence_interval': 99% confidence interval for the average serialization time.
                - 'deserialization_confidence_interval': 99% confidence interval for the average deserialization time.
    """
    p = subprocess.Popen(
                ["./perf", "script", "-i", perf_data, "-s", node2_brt_script]
            )
    sts = os.waitpid(p.pid, 0)

    df = pd.read_json("durations.txt")

    n = 1000
    list_df = [df[i : i + n] for i in range(0, df.shape[0], n)]

    # Pop the last entry, as the bundle which initiates the
    # node shutdown is also recorded, resulting in 16 list elements.
    list_df.pop()

    bundle_retention = []
    for data in list_df:
        bundle_retention.append(data["bundle_retention_time"].mean())

    serialization = []
    for data in list_df:
        serialization.append(data["serialization_time"].mean())

    deserialization = []
    for data in list_df:
        deserialization.append(data["deserialization_time"].mean())

    # Calculate 99% confidence intervals for all three values
    brt_confidence_interval = []
    for data in list_df:
        n_list = data["bundle_retention_time"].count()
        sem_list = data["bundle_retention_time"].std() / np.sqrt(n_list)
        t_value_99 = st.t.ppf(1 - (1 - 0.99) / 2, df=n_list - 1)
        brt_confidence_interval.append(t_value_99 * sem_list)

    ser_confidence_interval = []
    for data in list_df:
        n_list = data["serialization_time"].count()
        sem_list = data["serialization_time"].std() / np.sqrt(n_list)
        t_value_99 = st.t.ppf(1 - (1 - 0.99) / 2, df=n_list - 1)
        ser_confidence_interval.append(t_value_99 * sem_list)

    deser_confidence_interval = []
    for data in list_df:
        n_list = data["deserialization_time"].count()
        sem_list = data["deserialization_time"].std() / np.sqrt(n_list)
        t_value_99 = st.t.ppf(1 - (1 - 0.99) / 2, df=n_list - 1)
        deser_confidence_interval.append(t_value_99 * sem_list)

    data = np.array(
        [
            [
                "",
                "1",
                "10",
                "25",
                "50",
                "75",
                "100",
                "200",
                "300",
                "400",
                "500",
                "600",
                "700",
                "800",
                "900",
                "1000",
            ],
            ["avg_bundle_retention_time"] + bundle_retention,
            ["avg_serialization_time"] + serialization,
            ["avg_deserialization_time"] + deserialization,
            ["bundle_retention_confidence_interval"] + brt_confidence_interval,
            ["serialization_confidence_interval"] + ser_confidence_interval,
            ["deserialization_confidence_interval"] + deser_confidence_interval,
        ]
    )

    return pd.DataFrame(data=data[1:, 1:], index=data[1:, 0], columns=data[0, 1:])
