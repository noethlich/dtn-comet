from typing import List
from fastapi import UploadFile
from pydantic import BaseModel


class EvaluationRequest():
    def __init__(self, id: str, filename: str, filepath: str) -> None:
        self.id = id
        self.filename = filename
        self.filepath = filepath

class APIKeyRequest(BaseModel):
    key_name: str