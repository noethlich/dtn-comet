import sqlite3
from contextlib import contextmanager


class ConnectionPool:
    def __init__(self, db_path):
        self.db_path = db_path
        self.pool = []

    def acquire(self):
        if len(self.pool) == 0:
            conn = sqlite3.connect(self.db_path)
            conn.row_factory = sqlite3.Row # enable name-based access to columns (see: https://docs.python.org/3.5/library/sqlite3.html#sqlite3.Connection.row_factory)
        else:
            conn = self.pool.pop()

        return conn

    def release(self, conn):
        self.pool.append(conn)

@contextmanager
def connection_manager(db_path):
    pool = ConnectionPool(db_path)
    conn = pool.acquire()
    try:
        yield conn
    finally:
        pool.release(conn)