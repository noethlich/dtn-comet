import logging
import sys
import datetime
from typing import List
import uuid

from cachetools import TTLCache, cached
from utils.custom_logger import CustomFormatter
from utils.database_utils import connection_manager

# Setup logging
log_handler = logging.StreamHandler(sys.stdout)
log_handler.setLevel(logging.DEBUG)
log_handler.setFormatter(CustomFormatter())

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.propagate = False
logger.addHandler(log_handler)

class APIKeyStore:
    def __init__(self):
        self.db_path = "api_keys.db"

    def create_api_key(self, key_name: str) -> dict:
        with connection_manager(self.db_path) as conn:
            cur = conn.cursor()
            logger.debug("Processing registration request for key %s", key_name)
        
            api_key = str(uuid.uuid4())
            logger.debug("Generated API key")
            current_datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            cur.execute("INSERT INTO keys (api_key, created_at, key_name) VALUES (?,?,?)", (api_key, current_datetime, key_name))
            conn.commit()
            logger.debug("Added key to database.")

            return {"key_name": key_name, "api_key": api_key, "created_at": current_datetime}  

    def delete_api_key(self, key: str):
        with connection_manager(self.db_path) as conn:
            cur = conn.cursor()
            logger.debug("Processing deletion request for key %s", key)

            cur.execute("DELETE FROM keys WHERE key_name LIKE (?)", (key,))
            conn.commit()
            logger.debug("Deleted key.")

    @cached(cache=TTLCache(maxsize=1024, ttl=60))
    def api_key_exists(self, api_key: str) -> bool:
        with connection_manager(self.db_path) as conn:
            cur = conn.cursor()

            cur.execute("SELECT * FROM keys WHERE api_key=?", (api_key,))
            res = cur.fetchone()
            if res:
                logger.debug("Validated API key via database fetch!")
                return True

            return False
    
    def get_all_api_keys(self) -> List[dict]:
        with connection_manager(self.db_path) as conn:
            cur = conn.cursor()
            cur.execute("SELECT * FROM keys")
            res = cur.fetchall()
            return res


api_key_store = APIKeyStore()
