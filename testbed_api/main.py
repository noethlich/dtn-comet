import asyncio
import concurrent.futures
import logging
import os
import shutil
import subprocess
from datetime import datetime
from time import sleep
from typing import Annotated, List
from authentication.api_key_store import api_key_store
from results.results_store import results_store
from fastapi import (
    Body,
    Depends,
    FastAPI,
    File,
    HTTPException,
    Security,
    UploadFile,
    status,
)
from fastapi.security.api_key import APIKeyHeader
from pydantic import BaseModel
from utils.custom_logger import CustomFormatter
from utils.request_utils import EvaluationRequest, APIKeyRequest
from utils.result_processing import (
    process_cpu_memory_by_payload_size,
    process_goodput_by_payload_size,
    load_data,
    process_bundle_retention_times,
    process_rtt_measurements,
)

# Add IP adresses of testbed nodes here
TESTBED_NODES = [""]

log_handler = logging.StreamHandler()
log_handler.setLevel(logging.DEBUG)
log_handler.setFormatter(CustomFormatter())

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.propagate = False
logger.addHandler(log_handler)

evaluation_results = {}
requests = asyncio.Queue()

api_key_header = APIKeyHeader(name="x-api-key", auto_error=False)


class EvalRequestModel(BaseModel):
    request_id: str
    status: str
    queue_pos: int | None = None
    creation: datetime


app = FastAPI()


async def api_key_auth(api_key: str = Security(api_key_header)):
    if not api_key_store.api_key_exists(api_key):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
        )


async def copy_files(request):
    try:
        filepath = request.filepath

        for node in TESTBED_NODES:
            logger.debug("Copying configuration to node %s.", node)

            p = subprocess.Popen(
                ["scp", "-O", filepath, "root@" + node + ":~/recv_files/" + filepath]
            )
            sts = os.waitpid(p.pid, 0)

            if sts[1] != 0:
                logger.error(
                    "Could not copy file %s to node %s for request %s! scp exited with code %s.",
                    filepath,
                    node,
                    request.id,
                    sts[1],
                )
                return 1

    except Exception as e:
        logger.error(
            "Error when copying files for request " + request.id + ": " + str(e)
        )

    logger.info("Successfully copied files for request " + request.id + "!")
    logger.debug("Cleanup of files transmitted to testbed.")
    os.remove(filepath)
    return 0


async def worker():
    global evaluation_results

    while True:
        request = await requests.get()  # Get the next request from the queue

        logger.info("Starting evaluation process for request %s.", request.id)
        res = await copy_files(request)  # Process the request

        if res != 0:
            logger.warning(
                "Copying files failed for request %s, continuing with next request."
            )
            continue

        # Wait for all results to be received
        while len(evaluation_results) < len(TESTBED_NODES):
            await asyncio.sleep(10)
        await asyncio.sleep(10)

        logger.debug(
            "Processing results for request %s: %s", request.id, evaluation_results
        )
        node1_run = load_data(evaluation_results["1"][0])
        node1_stats = load_data(evaluation_results["1"][1])
        node2_run = load_data(evaluation_results["2"][0])
        node2_stats = load_data(evaluation_results["2"][1])
        node2_brts = evaluation_results["2"][2]

        mem_cpu = process_cpu_memory_by_payload_size(
            node1_run, node1_stats, node2_stats
        ).to_json()
        goodput = process_goodput_by_payload_size(node2_run).to_json()
        rtts = process_rtt_measurements(node1_run)
        brts = process_bundle_retention_times(node2_brts).to_json()

        results_store.save_evaluation(mem_cpu, goodput, rtts, brts)

        for file_paths in evaluation_results.values():
            for file_path in file_paths:
                try:
                    os.remove(file_path)
                    logger.debug(f"Deleted file: {file_path}")
                except FileNotFoundError:
                    logger.error(f"Could not delete {file_path}: File not found!")

        evaluation_results = {}
        logger.info("Finished cleanup for request %s.", request.id)


asyncio.create_task(worker())  # Start the worker task


# -------- RESULT HANDLING --------
@app.post(
    "/results", dependencies=[Depends(api_key_auth)], status_code=status.HTTP_200_OK
)
async def receive_evaluation_results(
    node_id: Annotated[str, Body()], files: List[UploadFile]
):
    """Endpoint used by the testbed nodes to submit the logs generated during the performance evaluation.

    Args:
        node_id (Annotated[str, Body): ID of the node submitting data.
        files (List[UploadFile]): A list of log files which can be processed to get performance data.
    """
    file_loc_list = []
    for file in files:
        file_location = f"files/{node_id}/{file.filename}"
        file_loc_list.append(file_location)
        with open(file_location, "wb+") as file_object:
            shutil.copyfileobj(file.file, file_object)
    evaluation_results[node_id] = file_loc_list


@app.get(
    "/results/{id}",
    dependencies=[Depends(api_key_auth)],
    status_code=status.HTTP_200_OK,
)
async def get_evaluation_result(id: str):
    """Endpoint for users to query the results of the evaluation with the given ID.

    Args:
        id (_type_): ID of the requested evaluation.

    Returns:
        JSON: JSON data structure containing the memory and CPU utilization, Goodputs, RTTs and bundle retention times measured during the evaluation.
    """
    return {"data": results_store.get_evaluation(id)}

@app.get(
    "/results",
    dependencies=[Depends(api_key_auth)],
    status_code=status.HTTP_200_OK,
)
async def get_all_evaluations():
    """Endpoint for users to query all evaluations.

    Returns:
        List[dict]: A list of all evaluations and the date they were created.
    """
    return {"data": results_store.get_all_evaluations()}


@app.delete(
    "/results/{id}",
    dependencies=[Depends(api_key_auth)],
    status_code=status.HTTP_200_OK,
)
async def delete_evaluation_results(id: str):
    """Endpoint for users to delete the results of a given evaluation.

    Args:
        id (str): The ID of the evaluation that should be deleted.
    """
    logger.info("Received deletion request for for evaluation %s", id)
    results_store.delete_evaluation(id)


# -------- EVALUATION HANDLING --------
@app.post(
    "/evaluation",
    dependencies=[Depends(api_key_auth)],
    status_code=status.HTTP_202_ACCEPTED,
)
async def enqueue_evaluation(payload: UploadFile) -> dict:
    """Endpoint for users to start a new evaluation. Evaluations will be enqueued and processed sequentially.

    Args:
        payload (UploadFile): A .tar.gz archive containing the framework configuration as well as optional configuration files for the DTN implementation.

    Raises:
        HTTPException: 500 Internal Server Error if archive could not be saved to disk.

    Returns:
        dict: Response containing the position in the queue as well as the request id.
    """
    import uuid

    request_id = uuid.uuid4()
    path = str(request_id) + ".tar.gz"

    request = EvaluationRequest(str(request_id), payload.filename, path)

    try:
        file_object = open(path, "wb+")
    except IOError as err:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Error while enqueuing evaluation request: " + err,
        )
    else:
        with file_object:
            file_object.write(payload.file.read())

    # Enqueue the request
    requests.put_nowait(request)

    return {
        "msg": "Successfully enqueued evaluation request!",
        "pos": requests.qsize(),
        "id": request_id,
    }


# -------- API KEY HANDLING --------
@app.post(
    "/api-keys",
    dependencies=[Depends(api_key_auth)],
    status_code=status.HTTP_201_CREATED,
)
def create_api_key(key_name: Annotated[str, Body()]) -> dict:
    """Endpoint for users to generate new API keys.

    Args:
        key_name (Annotated[str, Body): Name of the API key.

    Returns:
        dict: Dict containing the key name, the key and the creation timestamp.
    """
    logger.info("Received request for API key creation!")
    return api_key_store.create_api_key(key_name)


@app.delete(
    "/api-keys", dependencies=[Depends(api_key_auth)], status_code=status.HTTP_200_OK
)
def delete_api_key(key_name: Annotated[str, Body()]):
    """Endpoint for deleting API Keys

    Args:
         key_name (Annotated[str, Body): Name of the API key.
    """
    logger.info("Received deletion request for for key %s", key_name)
    api_key_store.delete_api_key(key_name)


@app.get("/api-keys", dependencies=[Depends(api_key_auth)])
def get_api_keys():
    """Endpoint for querying all API keys.

    Returns:
        List[dict]: List containing dicts of key name, key and creation timestamp
    """
    return api_key_store.get_all_api_keys()
