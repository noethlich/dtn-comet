# DTN-COMET Testbed API

## Requirements:    
- Python3
- SQLite3

## Setup
- Clone this repository
- Install the Python dependencies into a new virtual environment:
  ```
  python3 -m venv .venv && source .venv/bin/activate && pip install -r requirements.txt
  ```
- set up the SQLite databases needed for storing API keys and evaluations:
  *results.db*:  
  ``` 
  sqlite3 results.db
  ```
  ```sql
  CREATE TABLE "evaluations" (
  	"id"	INTEGER NOT NULL UNIQUE,
  	"date"	TEXT,
  	PRIMARY KEY("id" AUTOINCREMENT)
  );
  
  CREATE TABLE "testruns" (
  	"id"	INTEGER NOT NULL UNIQUE,
  	"evaluation_id"	INTEGER NOT NULL,
  	"type"	TEXT,
  	"data"	BLOB,
  	PRIMARY KEY("id" AUTOINCREMENT),
  	FOREIGN KEY("evaluation_id") REFERENCES "evaluations"("id") ON DELETE CASCADE
  );
  ```
  
  *api_keys.db*:  
  ``` 
  sqlite3 api_keys.db
  ```
  ```sql
  CREATE TABLE keys (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    api_key VARCHAR(255) NOT NULL,
    created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    key_name VARCHAR(255) NOT NULL
  );
  ```
- Add the IP addresses of the three testbed nodes to the `TESTBED_NODES` variable on l. 35 in the `main.py` file
- create a systemd service for running the API in the background   
  Example systemd unit file (replace the {variables}):  
  ```
  [Unit]
  Description=DTN-COMET API server
  
  [Service]
  WorkingDirectory=/home/{user}/dtn-comet/testbed_api/
  ExecStart=/home/{user}/dtn-comet/testbed_api/.venv/bin/uvicorn main:app --host 0.0.0.0 --log-config=log_conf.yaml
  Restart=always
  User={user}
  
  [Install]
  WantedBy=multi-user.target
  ```
- The interactive API endpoint documentation can be explored by navigating to `{API_NODE_IP}:8000/docs`

## Authentication
In order to use the API an API key is needed. You can create a temporary key for setting everything up using the following SQL statement with the `api_keys` database
```sql
INSERT INTO keys (api_key, created_at, key_name) VALUES ("setup-key", CURRENT_TIMESTAMP, "setup")
```
This key should then be removed once you have added a new key through the `/api-keys` endpoint.    
The API expects the API key to be sent using the `x-api-key` header. An example curl request to create a new API key would look like the following:
```
curl -XPOST -H "x-api-key: setup-key" -H 'Content-Type: application/json' -d '"{YOUR_KEY_NAME}"' API_IP:8000/api-keys
```
Alternatively you can use the interactive API endpoint documentation to add/delete keys.

## Evaluations
To start a new evaluation you can use the following request template:
```
curl -H "x-api-key: {API-KEY} -H 'Content-Type: multipart/form-data' -F 'payload=@{YOUR_CONFIG}.tar.gz;type=application/gzip' {API_IP}:8000/evaluation
```
Just like adding/deleting API Keys, it is also possible to start an evaluation through the interactive API endpoint documentation.



## Results
To query the results of an evaluation you can use the `/results/{id}` endpoint. ID's start at 1 and increase with every evaluation. Send a `GET` request to the `/results` endpoint to get a list of all evaluation ID's and the date these evaluations were processed at.

Results are returned as JSON with the following structure:
```JSON
"data":{
    "date": "...",
    "mem_cpu": {
        "1":{
            "cpu_usage_sender": [...],
            "memory_usage_sender": [...],
            "cpu_usage_receiver": [...],
            "memory_usage_receiver": [...]
        },
        "10":{
            "cpu_usage_sender": [...],
            "memory_usage_sender": [...],
            "cpu_usage_receiver": [...],
            "memory_usage_receiver": [...]
        },
        ...
        "1000":{
            "cpu_usage_sender": [...],
            "memory_usage_sender": [...],
            "cpu_usage_receiver": [...],
            "memory_usage_receiver": [...]
        }
    },
    "goodput":{
        "1": {
            "avg_goodput": "...",
            "confidence_interval": "..."
        },
        "10": {
            "avg_goodput": "...",
            "confidence_interval": "..."
        },
        ...
        "1000":{
            "avg_goodput": "...",
            "confidence_interval": "..."
        }
    },
    "rtts": [...],
    "brts":{
        "1": {
            "avg_bundle_retention_time": "...",
            "avg_serialization_time": "...",
            "avg_deserialization_time": "...",
            "bundle_retention_confidence": "...",
            "serialization_confidence": "...",
            "deserialization_confidence": "..."
        },
        "10": {
            "avg_bundle_retention_time": "...",
            "avg_serialization_time": "...",
            "avg_deserialization_time": "...",
            "bundle_retention_confidence": "...",
            "serialization_confidence": "...",
            "deserialization_confidence": "..."
        },
        ...
        "1000": {
            "avg_bundle_retention_time": "...",
            "avg_serialization_time": "...",
            "avg_deserialization_time": "...",
            "bundle_retention_confidence_interval": "...",
            "serialization_confidence_interval": "...",
            "deserialization_confidence_interval": "..."
        },
    }
}
``` 
Except for the round-trip times, all metrics are recorded for the following bundle payload sizes:
- 1 KB
- 10 KB
- 25 KB
- 50 KB
- 75 KB
- 100 KB to 1000 KB in steps of 100 KB
