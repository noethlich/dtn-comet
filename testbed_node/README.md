# DTN-COMET Testbed Node

## Requirements:    
- Python3
- perf
- docker
- inotifywait

## Setup

- Copy the `testbed_node` directory to all three testbed nodes
- [Install docker](https://docs.docker.com/engine/install/) and optionally [set it up, so you can run it as non-root user without sudo](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) (the testbed scripts assume they are run as root).
	- if you are using a Zynq UltraScale+ MPSoC device you can follow https://github.com/mayueanyou/meta-xilinx-docker/tree/mainzy

- Download and build perf (based on [this](https://medium.com/@manas.marwah/building-perf-tool-fc838f084f71))
	- Find kernel version:   
		  ```
		  uname -r
		  ```
	- Get kernel source:   
	  ```
	  curl -o linux-source.{VERSION}.tar.gz https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-{VERSION}.tar.gz
	  ```
	- Unpack:
	  ```
	  tar -xvf linux-source.{VERSION}.tar.gz
	  ```
	- Install build dependencies:
	  ```
	  sudo apt install make gcc flex bison pkg-config
	  ```
	- Install perf dependencies:
	  ```
	  sudo apt install libzstd1 libdwarf-dev libdw-dev binutils-dev libcap-dev libelf-dev libnuma-dev python3 python3-dev libssl-dev libunwind-dev libdwarf-dev zlib1g-dev liblzma-dev libaio-dev libtraceevent-dev debuginfod libpfm4-dev libslang2-dev systemtap-sdt-dev libperl-dev binutils-dev libbabeltrace-dev libiberty-dev libzstd-dev
	  ```
	- Make perf:
	  ```
	  cd linux-source.{VERSION}/tools/perf && make
	  ```
	- copy it into `/usr/local/bin/`:
	    
	  ```
	  sudo cp perf /usr/local/bin/
	  ```
- Install inotifywait
	- Package manager:
		    
	  ```
	  sudo apt-get install inotify-tools 
	  ```
	- if there is no package (e.g. on petalinux) follow https://docs.rockylinux.org/books/learning_rsync/06_rsync_inotify/
- Create directory into which user-submitted archives are copied (the default location the API will try to copy the configurations to is `root@{NODE_IP}:~/recv_files`)
- Create a systemd-service to continuously watch the directory and manage the evaluations:
	- /lib/system/systemd/testbed_watcher.service:
	  ```
	  [Unit]
	  Description=Service watching for submitted configurations in order to start performance evaluations.
		  
	  [Service]
	  EnvironmentFile=/etc/testbed_watcher_env
	  ExecStart=/home/path/to/watcher.sh
		  
	  [Install]
	  WantedBy=multi-user.target
	  ```
	- /etc/testbed_watcher_env:
		    
	  ```
	  TESTBED_NODE_NUMBER={1,2,3}
	  TESTBED_API_IP="XXX.XXX.XXX.XXX:8000"
	  TESTBED_API_KEY="AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE"
	  TESTBED_MONITOR_DIRECTORY="/home/{username}/recv_files"
	  TESTBED_SCRIPT_DIRECTORY="/home/{username}/dtn-comet/testbed_node"
	  ```
		- `TESTBED_NODE_NUMBER`: Configures which node the current machine represents (1⇿2⇿3). Needed by the template engine to generate the correct files and by the watcher.sh when uploading the results.
		- `TESTBED_API_IP`: IP adress under which the API node is reachable. Used by the nodes to upload the evaluation results.
		- `TESTBED_API_KEY`: The nodes have to authenticate using an API key, or they won't be able to upload results.
		- `TESTBED_MONITOR_DIRECTORY`: Full path to the directory into which the API node will copy the archive containing the configuration. Used to instruct inotifywait which directroy it should watch.
		- `TESTBED_SCRIPT_DIRECTORY`: Full path to the directory containing all scripts and templates (in this case the `testbed_node` directory).
	- Start the service and enable autostart on boot:
		    
	  ```
	  sudo systemctl start testbed_watcher
	  sudo systemctl enable testbed_watcher
	  ```

### Important
- **cgroup v2 vs v1**
	- I built the testbed using ZUBoard 1CG MPSoCs as testbed nodes running cgroup v1
	- the `monitor-cpu-memory.sh` script is therefore written in a way that *only* supports cgroup v1 for getting memory and CPU data
	- there are some differences between v1 and v2 which require modification of the `monitor-cpu-memory.sh` script (list not exhaustive and most important *untested*):
		- basepath is different:
		    
		  ```shell
		  /sys/fs/cgroup/system.slice/docker-$container_id.scope/
		  ```
		- Memory:
			- both capture data in in memory.stat
			- cgroup v1 `cache` (+ `mapped_file`?) seems to equal cgroup v2 `file`
			- cgroup v1 `rss` seems to equal cgroup v2 `anon`
			- cgroup v1 `swap` seems to equal cgroup v2 `memory.swap.current` (*not in memory.stat!*)
		- CPU:
			- v1's `cpuacct.usage` seems to be represented by v2's `usage_usec` in `cpu.stat`, however `cpuacct.usage` is recorded in nanoseconds instead of microseconds
