#!/bin/bash

output_file="docker_stats.log"
container_name="testbed_sandbox"
prev_system_cpu_usage=0
system_cpu_usage=0
container_cpu_usage=0

# Function to check if the container is running
check_container_running() {
    prev_system_cpu_usage=$(cat /proc/stat | grep -w cpu | awk '{print $1 + $2 + $3 + $4 + $5 + $6 + $7 + $8}')
    docker inspect --format '{{.State.Running}}' "$container_name" 2>/dev/null
}

> "$output_file"

# Wait for the container to start running
echo "Waiting for container $container_name to start running..."
while [ "$(check_container_running)" != "true" ]; do
    sleep 1
done

container_id=$(docker inspect --format="{{.Id}}" $container_name) 
echo "prev_cpu_usage is: $prev_system_cpu_usage"
# Container is running, proceed with monitoring
echo "Container $container_name ($container_id) is now running."

memory_path=/sys/fs/cgroup/memory/docker/$container_id/memory.stat
echo "$memory_path"
cpu_path=/sys/fs/cgroup/cpuacct/docker/$container_id/cpuacct.usage

prev_container_cpu_usage=$(cat /sys/fs/cgroup/cpuacct/docker/$container_id/cpuacct.usage)

sleep 5
# Monitoring loop
while [[ -d "/sys/fs/cgroup/memory/docker/$container_id" ]]; do
  # Read values from memory.stat
  # Read the content of memory.stat into a variable
  memory_stats=$(cat $memory_path)

  # Extract values using grep with appropriate regular expressions
  cache=$(grep -w 'cache' <<< "$memory_stats" | awk '{print $2}')
  rss=$(grep -w 'rss' <<< "$memory_stats" | awk '{print $2}')
  swap=$(grep -w 'swap' <<< "$memory_stats" | awk '{print $2}')
  file=$(grep -w 'mapped_file' <<< "$memory_stats" | awk '{print $2}')
 
  # Check if any values are empty
  if [[ -z "$cache" || -z "$rss" || -z "$swap" ]]; then
    echo "Error: Could not read all values from memory.stat"
    exit 1
  fi

  # Calculate and print the sum
  sum_memory=$((cache + rss + swap))
  
  # Get cumulative cpu usage for container and system
  system_cpu_usage=$(cat /proc/stat | grep -w cpu | awk '{print $1 + $2 + $3 + $4 + $5 + $6 + $7 + $8}')
  system_cpu_usage=$(($system_cpu_usage*10000000))
  container_cpu_usage=$(cat /sys/fs/cgroup/cpuacct/docker/$container_id/cpuacct.usage)

  #echo "prev sys: $prev_system_cpu_usage, sys: $system_cpu_usage, prev cont: $prev_container_cpu_usage, cont: $container_cpu_usage"
  # Calculate the usage deltas
  container_delta=$((container_cpu_usage - prev_container_cpu_usage))
  system_delta=$((system_cpu_usage - prev_system_cpu_usage))
  
  # Check for division by zero (system not busy)
  if [[ $system_delta -eq 0 ]]; then
    cpu_usage=0
  else
    # Calculate CPU utilization percentage
    cpu_usage=$((100 * container_delta / system_delta))
  fi
 
  #echo "Writing log..." 
  echo "$(date +%s),$container_delta,$system_delta,0,0,$cache,$rss,$swap,$file" >> "$output_file" 
  
  prev_system_cpu_usage=$system_cpu_usage
  prev_container_cpu_usage=$container_cpu_usage
  # Sleep for a defined interval
  sleep 1
done

echo "Path to memory.stat no longer exists. Exiting script."
echo "Container $container_name has stopped."
