#!/bin/bash


keyword="$1"
echo "Running RTT grep looking for keyword $keyword"    
# Use grep and sed to extract the time value based on the provided keyword
while IFS= read -r log_line; do
    echo "$log_line"
    time_value=$(echo "$log_line" | grep -o "${keyword}[0-9.]*" | sed "s/${keyword}//")
    
    if [ -n "$time_value" ]; then
        echo "LATENCY $time_value s"
    else
        echo "Time not found in the log line."
    fi
done

