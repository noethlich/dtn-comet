
#!/bin/bash

process_name="__PROCESS_NAME__"


while IFS= read -r line; do
    if [[ "$line" =~ TERMINATE ]]; then
        kill -2 $(pgrep -x $process_name)
    else
        __COMMAND_STOP__
        sleep 5
        __COMMAND_START__
        sleep 10
        __COMMAND_CONTACT__
    fi
done
