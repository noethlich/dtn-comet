#!/bin/bash

process_name="__PROCESS_NAME__"

# Function to check if the process is active
check_process_active() {
    pgrep -x "$process_name" > /dev/null
}

# Wait for the process to become active
echo "Waiting for process $process_name to become active..."
while ! check_process_active; do
    sleep 1
done

# Once the process is active, attach perf record
pid=$(pgrep -x "$process_name")
echo "Attaching perf record to process $process_name (PID: $pid)"

# Start recording with perf record
perf record -e "__PROBES__" -o perf.data -p $pid

perf probe __PROBE_DELETION__
