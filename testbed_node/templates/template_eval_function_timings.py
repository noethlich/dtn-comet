import pandas as pd
import json


# Variables for calculating function timings
__EVAL_VARIABLES__

events = pd.DataFrame({})

def trace_end():
    pd.set_option('float_format', '{:f}'.format)

    events = pd.DataFrame({
__SELECT_RESULTS__
    })
    events /= 1e6
    with open('durations.txt', 'w') as convert_file:
        convert_file.write(json.dumps(events.to_dict()))

    print(events.describe())

__PROBES_BEGIN__

__PROBES_RETURN__
