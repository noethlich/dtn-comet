#!/bin/bash

echo "Running Framework!"

process=__PROCESS_NAME__
# reduce maximum buffer of open file descriptors, so ION doesn't hang on startup
# see https://sourceforge.net/p/ion-dtn/mailman/message/37781431/
ulimit -n 512 

sleep 20

__COMMAND_START__


# Sleep to allow startup to finish and perf to capture some data before running the next commands
sleep 20

__COMMAND_CONTACT__

__COMMAND_ECHO__

__COMMAND_RECEIVE__


sleep 20


 __COMMAND_START__

# Capture the PID of the process
pid=$!

# Sleep to allow startup to finish and perf to capture some data before running the next commands
sleep 20

__COMMAND_CONTACT__

__COMMAND_ECHO__

__COMMAND_RECEIVE__

kill -s SIGINT pid


sleep 15

echo "Done!"

exit
