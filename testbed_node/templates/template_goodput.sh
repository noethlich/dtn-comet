#!/bin/bash

taskset -c 1 __COMMAND_START__  
sleep 5           
__COMMAND_CONTACT__
sleep 10
echo "==========STARTING GOODPUT CALCULATION=========="

for y in {1..2}
do
    for i in {1..10}
    do
        echo "==========1KB RUN #$i=========="
        echo "START1:$(date +%s)"
        __COMMAND_CONTINUOUS_BUNDLES_1KB__
    
        sleep 35 
        #60 
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__
    
        __COMMAND_STOP__
        sleep 5
        #20
        taskset -c 1 __COMMAND_START__
        sleep 5
        #10
        __COMMAND_CONTACT__
    done

    for i in {1..10}
    do
        echo "==========10KB RUN #$i=========="
        echo "START2:$(date +%s)"
        __COMMAND_CONTINUOUS_BUNDLES_10KB__
        
        sleep 35
    
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__

        __COMMAND_STOP__
        sleep 5
        taskset -c 1 __COMMAND_START__
        sleep 5
        __COMMAND_CONTACT__
    done
    
    for i in {1..10}
    do
        echo "==========25KB RUN #$i=========="
        echo "START1:$(date +%s)"
        __COMMAND_CONTINUOUS_BUNDLES_25KB__

        sleep 35

        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__

        __COMMAND_STOP__
        sleep 5
        taskset -c 1 __COMMAND_START__
        sleep 5
        __COMMAND_CONTACT__
    done

    for i in {1..10}
    do
        echo "==========50KB RUN #$i=========="
        echo "START1:$(date +%s)"
        __COMMAND_CONTINUOUS_BUNDLES_50KB__

        sleep 35

        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__

        __COMMAND_STOP__
        sleep 5
        taskset -c 1 __COMMAND_START__
        sleep 5
        __COMMAND_CONTACT__
    done

    for i in {1..10}
    do
        echo "==========75KB RUN #$i=========="
        echo "START1:$(date +%s)"
        __COMMAND_CONTINUOUS_BUNDLES_75KB__

        sleep 35

        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__

        __COMMAND_STOP__
        sleep 5
        taskset -c 1 __COMMAND_START__
        sleep 5
        __COMMAND_CONTACT__
    done

    for i in {1..10}
    do
        echo "==========100KB RUN #$i=========="
        echo "START3:$(date +%s)"
        __COMMAND_CONTINUOUS_BUNDLES_100KB__
        
        sleep 35
    
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__
    
        __COMMAND_STOP__
        sleep 5
        taskset -c 1 __COMMAND_START__
        sleep 5
        __COMMAND_CONTACT__
    done

    for i in {1..10}                                        
    do                                                      
        echo "==========200KB RUN #$i=========="            
        echo "START4:$(date +%s)"                           
        __COMMAND_CONTINUOUS_BUNDLES_200KB__                
                                                            
        sleep 35                                            
                                                            
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__ 
                                                            
        __COMMAND_STOP__                                    
        sleep 5                                            
        taskset -c 1 __COMMAND_START__                      
        sleep 5                                            
        __COMMAND_CONTACT__                                 
    done

    for i in {1..10}                                        
    do                                                      
        echo "==========300KB RUN #$i=========="            
        echo "START5:$(date +%s)"                           
        __COMMAND_CONTINUOUS_BUNDLES_300KB__                
                                                            
        sleep 35                                            
                                                            
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__ 
                                                            
        __COMMAND_STOP__                                    
        sleep 5                                            
        taskset -c 1 __COMMAND_START__                      
        sleep 5                                            
        __COMMAND_CONTACT__                                 
    done

    for i in {1..10}                                        
    do                                                      
        echo "==========400KB RUN #$i=========="            
        echo "START6:$(date +%s)"                           
        __COMMAND_CONTINUOUS_BUNDLES_400KB__                
                                                            
        sleep 35                                            
                                                            
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__ 
                                                            
        __COMMAND_STOP__                                    
        sleep 5                                            
        taskset -c 1 __COMMAND_START__                      
        sleep 5                                            
        __COMMAND_CONTACT__                                 
    done

    for i in {1..10}                                        
    do                                                      
        echo "==========500KB RUN #$i=========="            
        echo "START7:$(date +%s)"                           
        __COMMAND_CONTINUOUS_BUNDLES_500KB__                
                                                            
        sleep 35                                            
                                                            
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__ 
                                                            
        __COMMAND_STOP__                                    
        sleep 5                                            
        taskset -c 1 __COMMAND_START__                      
        sleep 5                                            
        __COMMAND_CONTACT__                                 
    done    

    for i in {1..10}                                        
    do                                                      
        echo "==========600KB RUN #$i=========="            
        echo "START8:$(date +%s)"                           
        __COMMAND_CONTINUOUS_BUNDLES_600KB__                
                                                            
        sleep 35                                            
                                                            
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__ 
                                                            
        __COMMAND_STOP__                                    
        sleep 5                                            
        taskset -c 1 __COMMAND_START__                      
        sleep 5                                            
        __COMMAND_CONTACT__                                 
    done

    for i in {1..10}                                        
    do                                                      
        echo "==========700KB RUN #$i=========="            
        echo "START9:$(date +%s)"                           
        __COMMAND_CONTINUOUS_BUNDLES_700KB__                
                                                            
        sleep 35                                            
                                                            
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__ 
                                                            
        __COMMAND_STOP__                                    
        sleep 5                                            
        taskset -c 1 __COMMAND_START__                      
        sleep 5                                            
        __COMMAND_CONTACT__                                 
    done

    for i in {1..10}                                        
    do                                                      
        echo "==========800KB RUN #$i=========="            
        echo "START10:$(date +%s)"                           
        __COMMAND_CONTINUOUS_BUNDLES_800KB__                
                                                            
        sleep 35                                            
                                                            
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__ 
                                                            
        __COMMAND_STOP__                                    
        sleep 5                                            
        taskset -c 1 __COMMAND_START__                      
        sleep 5                                            
        __COMMAND_CONTACT__                                 
    done
    
    for i in {1..10}                                        
    do                                                      
        echo "==========900KB RUN #$i=========="            
        echo "START11:$(date +%s)"                           
        __COMMAND_CONTINUOUS_BUNDLES_900KB__                
                                                            
        sleep 35                                            
                                                            
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__ 
                                                            
        __COMMAND_STOP__                                    
        sleep 5                                            
        taskset -c 1 __COMMAND_START__                      
        sleep 5                                            
        __COMMAND_CONTACT__                                 
    done

    for i in {1..10}
    do
        echo "==========1MB RUN #$i=========="
        echo "START12:$(date +%s)"
        __COMMAND_CONTINUOUS_BUNDLES_1MB__
       
        sleep 35
      
        echo "========RESTARTING NODES========"
        echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__
    
        __COMMAND_STOP__
        sleep 5
        taskset -c 1 __COMMAND_START__
        sleep 5
        __COMMAND_CONTACT__
    done
done
sleep 25 
echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N3__
echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__ 
sleep 10
__COMMAND_STOP__

sleep 20
