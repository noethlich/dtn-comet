#!/bin/bash

sleep 10
echo "==========STARTING CALCULATION OF BUNDLE RETENTION TIMES=========="


echo "==========1KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "1KB $i"
    __COMMAND_BUNDLE_1KB__
    sleep 1 
done    
 

echo "==========10KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "10KB $i" 
    __COMMAND_BUNDLE_10KB__
    sleep 1
done
  

echo "==========25KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do    
    echo "25KB $i" 
    __COMMAND_BUNDLE_25KB__
    sleep 1
done    

echo "==========50KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "50KB $i" 
    __COMMAND_BUNDLE_50KB__
    sleep 1
done    


echo "==========75KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do    
    echo "75KB $i" 
    __COMMAND_BUNDLE_75KB__
    sleep 1
done    


echo "==========100KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "100KB $i" 
    __COMMAND_BUNDLE_100KB__
    sleep 1
done    


echo "==========200KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "200KB $i" 
    __COMMAND_BUNDLE_200KB__
    sleep 1
done    
 

echo "==========300KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "300KB $i" 
    __COMMAND_BUNDLE_300KB__
    sleep 1
done    


echo "==========400KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do  
    echo "400KB $i" 
    __COMMAND_BUNDLE_400KB__
    sleep 1
done    


echo "==========500KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "500KB $i" 
    __COMMAND_BUNDLE_500KB__
    sleep 1
done    


echo "==========600KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do    
    echo "600KB $i" 
    __COMMAND_BUNDLE_600KB__
    sleep 1
done    


echo "==========700KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "700KB $i" 
    __COMMAND_BUNDLE_700KB__
    sleep 1
done    


echo "==========800KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "800KB $i" 
    __COMMAND_BUNDLE_800KB__
    sleep 1
done    


echo "==========900KB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "900KB $i" 
    __COMMAND_BUNDLE_900KB__
    sleep 1
done    


echo "==========1MB RUN=========="
echo "START1:$(date +%s)"
for i in {1..1000}
do
    echo "1MB $i" 
    __COMMAND_BUNDLE_1MB__
    sleep 1
done    

echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N3__
echo "PAYLOAD:RESTART" | __COMMAND_SEND_BUNDLE_N2__ 
sleep 5
__COMMAND_STOP__



