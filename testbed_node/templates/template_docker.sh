#!/bin/bash

docker build --network host -t eval_image .

./monitor-cpu-memory.sh &

echo "Starting container for RTT, Goodput, Memory and CPU Usage measurements..."
docker run --cpuset-cpus="1" --cap-add SYS_ADMIN --dns 8.8.8.8 --name testbed_sandbox __PORTS__ -p 5200:5200 --privileged eval_image

docker logs -f testbed_sandbox &> run.log  

sleep 30

docker container rm testbed_sandbox
docker rmi eval_image

docker build --network host -t eval_image -f Dockerfile_2nd_run .

./perf-script.sh &

echo "Starting container for measuring Bundle Retention Time..."
docker run --cpuset-cpus="1" --cap-add SYS_ADMIN --dns 8.8.8.8 --name testbed_sandbox __PORTS__ --privileged eval_image

docker container rm testbed_sandbox
docker rmi eval_image
