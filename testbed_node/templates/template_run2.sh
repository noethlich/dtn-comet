#!/bin/bash

echo "Running Framework!"

__COMMAND_PROBE__


process=__PROCESS_NAME__
# reduce maximum buffer of open file descriptors, so ION doesn't hang on startup
# see https://sourceforge.net/p/ion-dtn/mailman/message/37781431/
ulimit -n 512 

__COMMAND_START__

# Capture the PID of the process perf attaches to
perf_pid=$(pgrep -x $process)

# Sleep to allow perf to capture some data before running the next commands
sleep 45
echo "Setting up contacts."
__COMMAND_CONTACT__

sleep 45

__COMMAND_ECHO__

__COMMAND_RECEIVE__

__SCRIPT_BUNDLE_RETENTION__

echo "Killing process $perf_pid..."
kill -s SIGINT $perf_pid

wait $perf_pid

sleep 15

echo "Done!"

exit
