#!/bin/bash

# FIXME: 
# At the moment there is no explicit synchronization on the status of the 
# DTN implementation between the nodes. This means that if some timing is 
# off, the whole evaluation could fail. In the future this synchronization
# between the nodes needs to be implemented to guarantee stable processing.


echo "Running Framework!"

process=__PROCESS_NAME__

# reduce maximum buffer of open file descriptors, so ION doesn't hang on startup
# see https://sourceforge.net/p/ion-dtn/mailman/message/37781431/
ulimit -n 512 

sleep 20

__COMMAND_START__


sleep 45
echo "Setting up contacts (if needed)."
__COMMAND_CONTACT__

sleep 45

__COMMAND_ECHO__

__COMMAND_RECEIVE__

__SCRIPT_LATENCY__

# Wait for the other nodes to restart
sleep 20

__SCRIPT_THROUGHPUT__


sleep 15

echo "Done!"

exit
