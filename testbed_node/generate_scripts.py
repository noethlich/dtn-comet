import json
from collections import defaultdict
import sys
import os

NODE = os.environ['TESTBED_NODE_NUMBER']
PROBES = {}
perf_cmd = ""


def read_template(template_path):
    print("Reading template {template}".format(template=template_path))
    with open(template_path, "r") as template_file:
        template_content = template_file.read()

    return template_content


def read_config(config_path):
    print("Reading config {config}".format(config=config_path))
    with open(config_path, "r") as config_file:
        config_data = json.load(config_file)

    return config_data


def write_script(output_path, content):
    print("Writing file {out}".format(out=output_path))
    with open(output_path, "w") as output_file:
        output_file.write(content)


def generate_run_script(template_path, config_data, output_path):
    global perf_cmd
    print("Generating run.sh")
    template_content = read_template(template_path)
    function_dict = defaultdict(list)

    if not PROBES:
        ser = config_data.get("probe_serialize")
        deser = config_data.get("probe_deserialize")
        if ser:
            args = ser.split(":", 1)
            binary_or_lib, function_name = args[0], args[1]
            function_dict[binary_or_lib].append(function_name)

            PROBES["serialize_func_entry"] = "probe_{name}:{function}".format(
                name=binary_or_lib.split("/")[-1].split(".")[0],
                function=function_name.replace(":", "__L").split(";")[0].split("+")[0],
            )
            PROBES["serialize_func_return"] = "probe_{name}:{function}__return".format(
                name=binary_or_lib.split("/")[-1].split(".")[0],
                function=function_name.split(":")[0].split(";")[0].split("+")[0],
            )

        if deser:
            args = deser.split(":", 1)
            binary_or_lib, function_name = args[0], args[1]
            function_dict[binary_or_lib].append(function_name)

            PROBES["deserialize_func_entry"] = "probe_{name}:{function}".format(
                name=binary_or_lib.split("/")[-1].split(".")[0],
                function=function_name.replace(":", "__L").split(";")[0].split("+")[0],
            )
            PROBES["deserialize_func_return"] = (
                "probe_{name}:{function}__return".format(
                    name=binary_or_lib.split("/")[-1].split(".")[0],
                    function=function_name.split(":")[0].split(";")[0].split("+")[0],
                )
            )

        perf_cmd = ""
        for binary in function_dict:
            perf_cmd += "perf probe -x " + binary + " "
            for function in function_dict[binary]:
                perf_cmd += '--add "{function}" --add "{function2}%return" '.format(
                    function=function,
                    function2=function.split(":")[0].split(";")[0].split("+")[0],
                )

            perf_cmd += "&& "
        function_dict = defaultdict(list)

        bundle_entry, bundle_exit = (
            config_data.get("probe_bundle_entry"),
            config_data.get("probe_bundle_exit"),
        )

        args = bundle_entry.split(":")
        binary_or_lib, function_name = args[0], args[1]
        function_dict[binary_or_lib].append(function_name)

        PROBES["entry"] = "probe_{name}:{function}".format(
            name=binary_or_lib.split("/")[-1].split(".")[0],
            function=function_name.replace("%", "__"),
        )

        args = bundle_exit.split(":")
        binary_or_lib, function_name = args[0], args[1]
        function_dict[binary_or_lib].append(function_name)

        PROBES["exit"] = "probe_{name}:{function}".format(
            name=binary_or_lib.split("/")[-1].split(".")[0],
            function=function_name.replace("%", "__"),
        )

        deser_start = config_data.get("probe_deserialize_start")
        deser_end = config_data.get("probe_deserialize_end")
        if deser_start and deser_end and not "deserialize_func_entry" in PROBES:
            args = deser_start.split(":")
            binary_or_lib, function_name = args[0], args[1]
            function_dict[binary_or_lib].append(function_name)

            PROBES["deserialize_start"] = "probe_{name}:{function}".format(
                name=binary_or_lib.split("/")[-1].split(".")[0],
                function=function_name.replace("%", "__"),
            )

            args = deser_end.split(":")
            binary_or_lib, function_name = args[0], args[1]
            function_dict[binary_or_lib].append(function_name)

            PROBES["deserialize_end"] = "probe_{name}:{function}".format(
                name=binary_or_lib.split("/")[-1].split(".")[0],
                function=function_name.replace("%", "__"),
            )

        ser_start = config_data.get("probe_serialize_start")
        ser_end = config_data.get("probe_serialize_end")

        if ser_start and ser_end and not "serialize_func_entry" in PROBES:
            args = ser_start.split(":")
            binary_or_lib, function_name = args[0], args[1]
            function_dict[binary_or_lib].append(function_name)

            PROBES["serialize_start"] = "probe_{name}:{function}".format(
                name=binary_or_lib.split("/")[-1].split(".")[0],
                function=function_name.replace("%", "__"),
            )

            args = ser_end.split(":")
            binary_or_lib, function_name = args[0], args[1]
            function_dict[binary_or_lib].append(function_name)

            PROBES["serialize_end"] = "probe_{name}:{function}".format(
                name=binary_or_lib.split("/")[-1].split(".")[0],
                function=function_name.replace("%", "__"),
            )

        added = set()
        for binary in function_dict:
            perf_cmd += "perf probe -x " + binary + " "
            for function in function_dict[binary]:
                if not function in added:
                    perf_cmd += '--add "{function}" '.format(function=function)
                    added.add(function)

            perf_cmd += "&& "
        perf_cmd = perf_cmd[:-3]

    if NODE == "1":
        modified_content = (
            template_content.replace("__COMMAND_PROBE__", perf_cmd)
            .replace("__COMMAND_START__", str(config_data.get("command_run_n1")))
            .replace("__COMMAND_CONTACT__", str(config_data.get("command_contact_n1")))
            .replace("__COMMAND_RECEIVE__", "")
            .replace("__COMMAND_ECHO__", "")
            .replace("__SCRIPT_LATENCY__", "./framework/latency.sh")
            .replace("__SCRIPT_BUNDLE_RETENTION__", "./framework/bundle_retention.sh")
            .replace("__SCRIPT_THROUGHPUT__", "./framework/throughput.sh")
            .replace("__SCRIPT_CONTACT_UTIL__", "./framework/contact_utilization.sh")
            .replace("__PROCESS_NAME__", str(config_data.get("process_name")))
        )
    elif NODE == "2":
        modified_content = (
            template_content.replace("__COMMAND_PROBE__", perf_cmd)
            .replace("__COMMAND_START__", str(config_data.get("command_run_n2")))
            .replace(
                "__COMMAND_START_1S_N2__", str(config_data.get("command_run_n2_1s"))
            )
            .replace("__COMMAND_CONTACT__", str(config_data.get("command_contact_n2")))
            .replace(
                "__COMMAND_RECEIVE__",
                "{command} | ./framework/kill.sh".format(
                    command=str(config_data.get("command_receive_bundle_n2"))
                ),
            )
            .replace(
                "__COMMAND_RECEIVE_COUNT__", config_data.get("command_receive_count")
            )
            .replace("__COMMAND_STOP__", config_data.get("command_stop"))
            .replace(
                "__COMMAND_RECEIVE_CONTACT_UTIL__",
                "{command} | ./framework/contact_utilization_read.sh &".format(
                    command=config_data.get("command_receive_contact_util")
                ),
            )
            .replace(
                "__COMMAND_ECHO_N2__",
                "{command}".format(command=str(config_data.get("command_echo_n2"))),
            )
            .replace("__SCRIPT_LATENCY__", "")
            .replace("__SCRIPT_THROUGHPUT__", "")
            .replace("__SCRIPT_CONTACT_UTIL__", "")
            .replace("__PROCESS_NAME__", str(config_data.get("process_name")))
        )
    else:
        modified_content = (
            template_content.replace("__COMMAND_PROBE__", perf_cmd)
            .replace("__COMMAND_START__", str(config_data.get("command_run_n3")))
            .replace(
                "__COMMAND_RECEIVE__",
                "{command} | ./framework/kill.sh".format(
                    command=str(config_data.get("command_receive_bundle_n3"))
                ),
            )
            .replace("__COMMAND_CONTACT__", str(config_data.get("command_contact_n3")))
            .replace(
                "__COMMAND_ECHO__",
                "{command}".format(command=str(config_data.get("command_echo"))),
            )
            .replace("__COMMAND_RECEIVE_COUNT__", "")
            .replace("__SCRIPT_LATENCY__", "")
            .replace("__SCRIPT_THROUGHPUT__", "")
            .replace("__SCRIPT_CONTACT_UTIL__", "")
            .replace("__COMMAND_RECEIVE_CONTACT_UTIL__", "")
            .replace("__PROCESS_NAME__", str(config_data.get("process_name")))
        )

    write_script(output_path, modified_content)


def generate_perf_script(template_path, config_data, output_path):
    print("Generating perf-script.sh")
    template_content = read_template(template_path)

    del_probes = ""

    for probe in PROBES.values():
        del_probes += "--del {} ".format(probe)

    modified_content = (
        template_content.replace(
            "__PROCESS_NAME__", str(config_data.get("process_name"))
        )
        .replace("__PROBES__", ",".join(PROBES.values()))
        .replace("__PROBE_DELETION__", del_probes)
    )

    write_script(output_path, modified_content)


def generate_dockerfile(template_path, config_path, output_path):
    print("Generating Dockerfile")
    template_content = read_template(template_path)
    config_string = ""

    if config_data.get("uses_config_file"):
        if NODE == "1":
            config_list = config_data.get("name_config_n1").split(",")
            config_list = ["COPY " + c + " / \n" for c in config_list]
            config_string = "".join(config_list)

        elif NODE == "2":
            config_list = config_data.get("name_config_n2").split(",")
            config_list = ["COPY " + c + " / \n" for c in config_list]
            config_string = "".join(config_list)
        else:
            config_list = config_data.get("name_config_n3").split(",")
            config_list = ["COPY " + c + " / \n" for c in config_list]
            config_string = "".join(config_list)

    modified_content = template_content.replace(
        "__DOCKER_IMAGE__", str(config_data.get("docker_image"))
    ).replace("__COPY_CONFIG__", config_string)
    write_script(output_path, modified_content)


def generate_evaluation_script(template_path, config_data, output_path):
    print("Generating eval_function_timings.py")
    template_content = read_template(template_path)

    variables = ""
    function_entry = ""
    function_return = ""
    results = ""

    variables += "ser_start = 0\n"
    variables += "ser_time = []\n"
    results += "       'serialization_time': pd.Series(ser_time),\n"
    variables += "bundle_entry = 0\n"
    variables += "bundle_retention_time = []\n"
    results += "       'bundle_retention_time': pd.Series(bundle_retention_time),\n"
    variables += "deser_start = 0\n"
    variables += "deser_time = []\n"
    results += "       'deserialization_time': pd.Series(deser_time),\n"
    
    # Differentiate between a serialization process spanning a single function and
    # multiple functions
    if "serialize_func_entry" in PROBES:
        probe = PROBES["serialize_func_entry"]
        function_entry += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_ip, perf_sample_dict,):\n".format(
            probe.replace(":", "__")
        )
        function_entry += "    global ser_start\n"
        function_entry += "    ser_start = (common_secs * 1e9) + common_nsecs\n\n"

        probe = PROBES["serialize_func_return"]
        function_return += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_func, __probe_ret_ip, perf_sample_dict):\n".format(
            probe.replace(":", "__")
        )
        function_return += "    global ser_start, ser_time\n"
        function_return += "    end = (common_secs * 1e9) + common_nsecs\n"
        function_return += "    ser_time.append(end - ser_start)\n\n"
    elif "serialize_start" in PROBES:
        probe = PROBES["serialize_start"]
        function_entry += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_ip, perf_sample_dict,):\n".format(
            probe.replace(":", "__")
        )
        function_entry += "    global ser_start\n"
        function_entry += "    ser_start = (common_secs * 1e9) + common_nsecs\n\n"

        probe = PROBES["serialize_end"]
        if "__return" in probe:
            function_return += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_func, __probe_ret_ip, perf_sample_dict):\n".format(
                probe.replace(":", "__")
            )
            function_return += "    global ser_start, ser_time\n"
            function_return += "    end = (common_secs * 1e9) + common_nsecs\n"
            function_return += "    ser_time.append(end - ser_start)\n\n"
        else:
            function_entry += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_ip, perf_sample_dict,):\n".format(
                probe.replace(":", "__")
            )
            function_entry += "    global ser_start, ser_time\n"
            function_entry += "    end = (common_secs * 1e9) + common_nsecs\n"
            function_entry += "    ser_time.append(end - ser_start)\n\n"
    
    # Differentiate between a deserialization process spanning a single function (like in ION) and
    # multiple functions (like in µD3TN)
    if "deserialize_func_entry" in PROBES:
        probe = PROBES["deserialize_func_entry"]
        function_entry += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_ip, perf_sample_dict,):\n".format(
            probe.replace(":", "__")
        )
        function_entry += "    global deser_start\n"
        function_entry += "    deser_start = (common_secs * 1e9) + common_nsecs\n\n"

        probe = PROBES["deserialize_func_return"]
        function_return += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_func, __probe_ret_ip, perf_sample_dict):\n".format(
            probe.replace(":", "__")
        )
        function_return += "    global deser_start, deser_time\n"
        function_return += "    end = (common_secs * 1e9) + common_nsecs\n"
        function_return += "    deser_time.append(end - deser_start)\n\n"
    elif "deserialize_start" in PROBES:
        probe = PROBES["deserialize_start"]
        function_entry += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_ip, perf_sample_dict,):\n".format(
            probe.replace(":", "__")
        )
        # It may happen that the function in which a bundle enters the DTN implementation also starts the deserialization.
        # If this is the case we have to handle both bundle entry time and deserialization start time in the same function.
        if probe == PROBES["entry"]:
            function_entry += "    global deser_start, bundle_entry\n"
            function_entry += "    deser_start = (common_secs * 1e9) + common_nsecs\n"
            function_entry += "    bundle_entry = (common_secs * 1e9) + common_nsecs\n\n"
        else:
            function_entry += "    global deser_start\n"
            function_entry += "    deser_start = (common_secs * 1e9) + common_nsecs\n\n"

        probe = PROBES["deserialize_end"]
        if "__return" in probe:
            function_return += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_func, __probe_ret_ip, perf_sample_dict):\n".format(
                probe.replace(":", "__")
            )
            function_return += "    global deser_start, deser_time\n"
            function_return += "    end = (common_secs * 1e9) + common_nsecs\n"
            function_return += "    deser_time.append(end - deser_start)\n\n"
        else:
            function_entry += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_ip, perf_sample_dict,):\n".format(
                probe.replace(":", "__")
            )
            function_entry += "    global deser_start, deser_time\n"
            function_entry += "    end = (common_secs * 1e9) + common_nsecs\n"
            function_entry += "    deser_time.append(end - deser_start)\n\n"
            

    if ("deserialize_start" in PROBES and PROBES["entry"] != PROBES["deserialize_start"]) or ("deserialize_func_entry" in PROBES and PROBES["entry"] != PROBES["deserialize_func_entry"]):
        probe = PROBES["entry"]
        function_entry += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_ip, perf_sample_dict,):\n".format(
            probe.replace(":", "__")
        )
        function_entry += "    global bundle_entry\n"
        function_entry += "    if bundle_entry == 0:\n"
        function_entry += "        bundle_entry = (common_secs * 1e9) + common_nsecs\n\n"

    probe = PROBES["exit"]
    # We can either specify that time should be measured until the bundle exit function returns,
    # or if there's no reason for that (e.g. because the function is a stub like in the TCPCLA µD3TN), until the function is called.
    if "__return" in probe:
        function_return += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_func, __probe_ret_ip, perf_sample_dict):\n".format(
            probe.replace(":", "__")
        )
        function_return += "    global bundle_entry, bundle_retention_time\n"
        function_return += "    end = (common_secs * 1e9) + common_nsecs\n"
        function_return += "    bundle_retention_time.append(end - bundle_entry)\n"
        function_return += "    bundle_entry = 0\n\n"
    else:
        function_entry += "def {}(event_name, context, common_cpu, common_secs, common_nsecs, common_pid, common_comm, common_callchain, __probe_ip, perf_sample_dict,):\n".format(
            probe.replace(":", "__")
        )
        function_entry += "    global bundle_entry, bundle_retention_time\n"
        function_entry += "    end = (common_secs * 1e9) + common_nsecs\n"
        function_entry += "    bundle_retention_time.append(end - bundle_entry)\n"
        function_entry += "    bundle_entry = 0\n\n"

    modified_content = (
        template_content.replace("__EVAL_VARIABLES__", variables)
        .replace("__SELECT_RESULTS__", results)
        .replace("__PROBES_BEGIN__", function_entry)
        .replace("__PROBES_RETURN__", function_return)
    )

    write_script(output_path, modified_content)


def generate_docker_script(template_path, config_data, output_path):
    print("Generating docker.sh")
    template_content = read_template(template_path)
    ports = [
        "-p {port}:{port}".format(port=port)
        for port in config_data.get("ports").split(",")
    ]

    modified_content = template_content.replace("__PORTS__", " ".join(ports))

    write_script(output_path, modified_content)


def generate_latency_script(template_path, config_data, output_path):
    print("Generating latency.sh")
    template_content = read_template(template_path)
    modified_content = (
        template_content.replace(
            "__COMMAND_PING__",
            "{command} 2>&1 | ./framework/rtt.sh {keyword}".format(
                command=str(config_data.get("command_ping")),
                keyword=str(config_data.get("ping_keyword")),
            ),
        )
        .replace("__COMMAND_STOP__", str(config_data.get("command_stop")))
        .replace(
            "__COMMAND_SEND_BUNDLE_N3__", str(config_data.get("command_send_bundle_n3"))
        )
        .replace(
            "__COMMAND_SEND_BUNDLE_N2__", str(config_data.get("command_send_bundle_n2"))
        )
    )
    write_script(output_path, modified_content)


def generate_throughput_script(template_path, config_data, output_path):
    print("Generating throughput.sh")
    template_content = read_template(template_path)
    modified_content = (
        template_content.replace(
            "__COMMAND_SEND_BUNDLE_N3__", str(config_data.get("command_send_bundle_n3"))
        )
        .replace(
            "__COMMAND_SEND_BUNDLE_N2__", str(config_data.get("command_send_bundle_n2"))
        )
        .replace("__COMMAND_START__", str(config_data.get("command_run_n1")))
        .replace("__COMMAND_CONTACT__", str(config_data.get("command_contact_n1")))
        .replace("__COMMAND_STOP__", str(config_data.get("command_stop")))
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_1KB__",
            str(config_data.get("command_continuous_bundles_1kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_10KB__",
            str(config_data.get("command_continuous_bundles_10kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_25KB__",
            str(config_data.get("command_continuous_bundles_25kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_50KB__",
            str(config_data.get("command_continuous_bundles_50kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_75KB__",
            str(config_data.get("command_continuous_bundles_75kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_100KB__",
            str(config_data.get("command_continuous_bundles_100kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_200KB__",
            str(config_data.get("command_continuous_bundles_200kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_300KB__",
            str(config_data.get("command_continuous_bundles_300kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_400KB__",
            str(config_data.get("command_continuous_bundles_400kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_500KB__",
            str(config_data.get("command_continuous_bundles_500kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_600KB__",
            str(config_data.get("command_continuous_bundles_600kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_700KB__",
            str(config_data.get("command_continuous_bundles_700kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_800KB__",
            str(config_data.get("command_continuous_bundles_800kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_900KB__",
            str(config_data.get("command_continuous_bundles_900kb")),
        )
        .replace(
            "__COMMAND_CONTINUOUS_BUNDLES_1MB__",
            str(config_data.get("command_continuous_bundles_1mb")),
        )
    )
    write_script(output_path, modified_content)

def generate_kill_script(template_path, config_data, output_path):
    print("Generating kill.sh")
    template_content = read_template(template_path)
    modified_content = template_content.replace(
        "__PROCESS_NAME__", str(config_data.get("process_name"))
    ).replace("__COMMAND_STOP__", str(config_data.get("command_stop")))
    write_script(output_path, modified_content)


def generate_bundle_retention_script(template_path, config_data, output_path):
    print("Generating bundle_retention.sh")
    template_content = read_template(template_path)
    modified_content = (
        template_content.replace(
            "__COMMAND_SEND_BUNDLE_N3__", str(config_data.get("command_send_bundle_n3"))
        )
        .replace(
            "__COMMAND_SEND_BUNDLE_N2__", str(config_data.get("command_send_bundle_n2"))
        )
        .replace("__COMMAND_START__", str(config_data.get("command_run_n1")))
        .replace("__COMMAND_CONTACT__", str(config_data.get("command_contact_n1")))
        .replace("__COMMAND_STOP__", str(config_data.get("command_stop")))
        .replace("__COMMAND_BUNDLE_1KB__", str(config_data.get("command_bundle_1kb")))
        .replace("__COMMAND_BUNDLE_10KB__", str(config_data.get("command_bundle_10kb")))
        .replace("__COMMAND_BUNDLE_25KB__", str(config_data.get("command_bundle_25kb")))
        .replace("__COMMAND_BUNDLE_50KB__", str(config_data.get("command_bundle_50kb")))
        .replace("__COMMAND_BUNDLE_75KB__", str(config_data.get("command_bundle_75kb")))
        .replace(
            "__COMMAND_BUNDLE_100KB__", str(config_data.get("command_bundle_100kb"))
        )
        .replace(
            "__COMMAND_BUNDLE_200KB__", str(config_data.get("command_bundle_200kb"))
        )
        .replace(
            "__COMMAND_BUNDLE_300KB__", str(config_data.get("command_bundle_300kb"))
        )
        .replace(
            "__COMMAND_BUNDLE_400KB__", str(config_data.get("command_bundle_400kb"))
        )
        .replace(
            "__COMMAND_BUNDLE_500KB__", str(config_data.get("command_bundle_500kb"))
        )
        .replace(
            "__COMMAND_BUNDLE_600KB__", str(config_data.get("command_bundle_600kb"))
        )
        .replace(
            "__COMMAND_BUNDLE_700KB__", str(config_data.get("command_bundle_700kb"))
        )
        .replace(
            "__COMMAND_BUNDLE_800KB__", str(config_data.get("command_bundle_800kb"))
        )
        .replace(
            "__COMMAND_BUNDLE_900KB__", str(config_data.get("command_bundle_900kb"))
        )
        .replace("__COMMAND_BUNDLE_1MB__", str(config_data.get("command_bundle_1mb")))
    )
    write_script(output_path, modified_content)


config_data = read_config("config.json")
if NODE == "1":
    generate_run_script("templates/template_run.sh", config_data, "framework/run.sh")
    generate_run_script("templates/template_run2.sh", config_data, "framework/run2.sh")
elif NODE == "2":
    generate_run_script("templates/template_run_n2.sh", config_data, "framework/run.sh")
    generate_run_script("templates/template_run2_n2.sh", config_data, "framework/run2.sh")
else:
    generate_run_script("templates/template_run_n3.sh", config_data, "framework/run.sh")
    generate_run_script("templates/template_run2_n3.sh", config_data, "framework/run2.sh")
generate_perf_script("templates/template_perf-script.sh", config_data, "perf-script.sh")
generate_dockerfile("templates/template_Dockerfile", config_data, "Dockerfile")
generate_dockerfile("templates/template_Dockerfile2", config_data, "Dockerfile_2nd_run")
generate_evaluation_script(
    "templates/template_eval_function_timings.py",
    config_data,
    "eval_function_timings.py",
)
generate_docker_script("templates/template_docker.sh", config_data, "docker.sh")
generate_latency_script(
    "templates/template_latency.sh", config_data, "framework/latency.sh"
)
generate_throughput_script(
    "templates/template_goodput.sh", config_data, "framework/throughput.sh"
)
generate_kill_script("templates/template_kill.sh", config_data, "framework/kill.sh")
generate_bundle_retention_script(
    "templates/template_bundle_retention.sh",
    config_data,
    "framework/bundle_retention.sh",
)
